//
//  ViewController.swift
//  Example-iOS
//
//  Created by Harvey on 2021/5/20.
//  Copyright © 2021 Harvey/姚作潘. All rights reserved.
//

import UIKit
import ElegantSnap
import SnapKit

func angleBetweenPoints(startPoint: CGPoint, endPoint: CGPoint) -> CGFloat {
   return atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x)
}

func angleBetweenVectors(vector1: (Double, Double), vector2: (Double, Double)) -> Double {
    let dotProduct = vector1.0 * vector2.0 + vector1.1 * vector2.1
    let normVector1 = sqrt(vector1.0 * vector1.0 + vector1.1 * vector1.1)
    let normVector2 = sqrt(vector2.0 * vector2.0 + vector2.1 * vector2.1)
    let angle = acos(dotProduct / (normVector1 * normVector2))
    return angle
}

class ViewController: UIViewController {

    let subView = UIView(frame: CGRect(x: 80, y: 200, width: 230, height: 230))
    let mathCircular = MathCircular(radius: 100.0, center: CGPoint(x: 115.0, y: 115.0), startPoint: CGPoint(x: 0.0, y: 100.0))
    let thicknessView = UIView()
    
    private lazy var animationLayer: CAShapeLayer = {
        createProgressLayer(strokeColor: .blue)
    }()

    private func createProgressLayer(strokeColor: UIColor) -> CAShapeLayer {
        let bezierPath = UIBezierPath(arcCenter: CGPoint(x: 115.0, y: 115.0),
                                      radius: radius - progressThickness/2.0 - 1.0,
                                      startAngle: -CGFloat.pi / 2.0,
                                      endAngle: CGFloat.pi * 1.5, clockwise: true)
        
        let animationLayer = CAShapeLayer()
        animationLayer.path = bezierPath.cgPath
        animationLayer.lineWidth = progressThickness
        animationLayer.lineJoin = .round
        animationLayer.lineCap = .round
        animationLayer.fillColor = UIColor.clear.cgColor
        animationLayer.strokeColor = strokeColor.cgColor
        return animationLayer
    }
    
    let radius = 100.0
    open var progressThickness: CGFloat { 20.0 }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        subView.backgroundColor = .red
        view.addSubview(subView)
        
        thicknessView.frame = CGRect(x: 109, y: 15 + 4.0, width: 12, height: 12)
        thicknessView.backgroundColor = .purple
        thicknessView.layer.cornerRadius = 6.0
        thicknessView.layer.zPosition = 1
        subView.addSubview(thicknessView)
        
        subView.layer.addSublayer(animationLayer)
    }
    
    lazy var KeyframeAnimation: CAKeyframeAnimation = {
        // 创建帧动画
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position"
        //去掉停顿效果（计算会有停顿）
        animation.calculationMode = .cubicPaced
        // 自动旋转
        animation.rotationMode = .rotateAutoReverse
        animation.isRemovedOnCompletion = false
        animation.fillMode = .both
        return animation
    }()
    
    func KeyframeAnimation(_ progressValue: Double) {
        let startAngle = CGFloat.pi * 1.5
        let endAngle = startAngle + Double.pi * 2.0 * progressValue
  
        // 创建路径
        let path = UIBezierPath(arcCenter: CGPoint(x: 115, y: 115),
                                radius: 90.0,
                                startAngle: endAngle - 0.001,
                                endAngle: endAngle,
                                clockwise: true)
        KeyframeAnimation.path = path.cgPath
        thicknessView.layer.add(KeyframeAnimation, forKey: "position")
    }
}

extension ViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: subView) else { return }
        let angle = mathCircular.angle(of: point)
        let progress = angle / (CGFloat.pi * 2.0)
        KeyframeAnimation(progress)
        print("夹角是: \(angle * 180.0 / CGFloat.pi), progress: \(progress)")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
}

struct MathCircular {
    let radius: CGFloat
    let center: CGPoint
    let startPoint: CGPoint
    let offset: CGFloat
    
    init(radius: CGFloat, center: CGPoint, startPoint: CGPoint) {
        self.radius = radius
        self.center = center
        self.startPoint = startPoint
        offset = center.x - radius
    }
    
    private func adapter(_ point: CGPoint) -> (CGPoint, CGFloat) {
        // 第一象限
        if point.x >= center.x, point.x < center.x + radius, point.y >= offset, point.y < center.y {
            return (CGPoint(x: point.x - center.x, y: center.y - point.y), 0.0)
        }
        
        // 第二象限
        if point.x >= center.x, point.x < center.x + radius, point.y >= center.y, point.y < radius + center.y {
            return (CGPoint(x: point.x - center.x, y: center.y - point.y), CGFloat.pi / 2.0)
        }
        
        // 第三象限
        if point.x >= offset, point.x < center.x, point.y >= center.y, point.y < radius + center.y {
            return (CGPoint(x: -(center.x - point.x), y: -(point.y - center.y)), CGFloat.pi)
        }
        
        // 第四象限
//        if point.x >= offset, point.x < center.x, point.y >= offset, point.y < center.y {
            return (CGPoint(x: -(center.x - point.x), y: center.y - point.y), CGFloat.pi * 1.5)
//        }
        
//        fatalError("MathCircular Error")
    }
    
    func angle(of point: CGPoint) -> CGFloat {
        let (point, baseAngle) = adapter(point)
        let angle = atan(point.x / point.y)
        print("point", calculatePoint(angle: angle, radius: 90.0))
        return angle > 0 ? baseAngle + angle : baseAngle + CGFloat.pi / 2.0 + angle
    }
    
    func calculatePoint(angle: Double, radius: Double) -> CGPoint {
        // 将角度从度转换为弧度
        let angleInRadians = 90.0 - abs(angle) * 180.0 / Double.pi
        print(angleInRadians)
        // 计算x和y的值
        let x = radius * cos(angleInRadians)
        let y = radius * sin(angleInRadians)
      
        return CGPoint(x: x, y: y)
    }
}
