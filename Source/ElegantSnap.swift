//
//  BriefSnap.swift
//  CCVideo
//
//  Created by Harvey on 2020/1/1.
//  Copyright © 2020 Harvey/姚作潘. All rights reserved.
//

import SnapKit

#if os(macOS)
import AppKit
public typealias ElegantView = NSView
#else
import UIKit
public typealias ElegantView = UIView
#endif

public class Layout {
    var relatableItems: [Item] = []
    
    @discardableResult
    public func add(_ item: Item) -> Self {
        relatableItems.append(item)
        return self
    }
    
    public static var relatable: Layout { .init() }
    
    public static var equalToSuperview: [Item] {
        [.top, .bottom, .leading, .trailing]
    }
}

public extension Layout {
    typealias RelatableTarget = ConstraintRelatableTarget
    typealias OffsetTarget = ConstraintOffsetTarget
}

public extension Layout {
    
    class Item {
        var equalTo: RelatableTarget? = nil
        @discardableResult
        public func equal(_ target: RelatableTarget) -> Self {
            self.equalTo = target
            return self
        }
        
        var offset: OffsetTarget? = nil
        @discardableResult
        public func offset(_ offset: OffsetTarget) -> Self {
            self.offset = offset
            return self
        }
        
        var attributes: Attributes
        init(_ attributes: Attributes) {
            self.attributes = attributes
        }
        
        public static var top: Item {
            Item(.top)
        }
        
        public static var size: Item {
            Item(.size)
        }
        
        public static var width: Item {
            Item(.width)
        }
        
        public static var edges: Item {
            Item(.edges)
        }
        
        public static var bottom: Item {
            Item(.bottom)
        }
        
        public static var center: Item {
            Item(.center)
        }
        
        public static var height: Item {
            Item(.height)
        }
        
        public static var leading: Item {
            Item(.leading)
        }
        
        public static var centerX: Item {
            Item(.centerX)
        }
        
        public static var centerY: Item {
            Item(.centerY)
        }
        
        public static var margins: Item {
            Item(.margins)
        }
        
        public static var trailing: Item {
            Item(.trailing)
        }
        
        public static var topMargin: Item {
            Item(.topMargin)
        }
        
        public static var leftMargin: Item {
            Item(.leftMargin)
        }
        
        public static var rightMargin: Item {
            Item(.rightMargin)
        }
        
        public static var lastBaseline: Item {
            Item(.lastBaseline)
        }
        
        public static var bottomMargin: Item {
            Item(.bottomMargin)
        }
        
        public static var firstBaseline: Item {
            Item(.firstBaseline)
        }
        
        public static var leadingMargin: Item {
            Item(.leadingMargin)
        }
        
        public static var trailingMargin: Item {
            Item(.trailingMargin)
        }
        
        public static var directionalEdges: Item {
            Item(.directionalEdges)
        }
        
        public static var directionalMargins: Item {
            Item(.directionalMargins)
        }
        
        public static var centerWithinMargins: Item {
            Item(.centerWithinMargins)
        }
        
        public static var centerXWithinMargins: Item {
            Item(.centerXWithinMargins)
        }
        
        public static var centerYWithinMargins: Item {
            Item(.centerYWithinMargins)
        }
        
        public static func width(_ equalTo: RelatableTarget) -> Item {
            width.equal(equalTo)
        }
        
        public static func height(_ equalTo: RelatableTarget) -> Item {
            height.equal(equalTo)
        }
        
        public static func size(_ width: CGFloat, _ height: CGFloat) -> Item {
            size.equal(CGSize(width: width, height: height))
        }
        
        public static func top(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? top.equal(equalTo) : top.equal(equalTo).offset(offset!)
        }
        
        public static func edges(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? edges.equal(equalTo) : edges.equal(equalTo).offset(offset!)
        }
        
        public static func bottom(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? bottom.equal(equalTo) : bottom.equal(equalTo).offset(offset!)
        }
        
        public static func center(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? center.equal(equalTo) : center.equal(equalTo).offset(offset!)
        }
        
        public static func leading(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? leading.equal(equalTo) : leading.equal(equalTo).offset(offset!)
        }
        
        public static func centerX(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? centerX.equal(equalTo) : centerX.equal(equalTo).offset(offset!)
        }
        
        public static func centerY(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? centerY.equal(equalTo) : centerY.equal(equalTo).offset(offset!)
        }
        
        public static func margins(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? margins.equal(equalTo) : margins.equal(equalTo).offset(offset!)
        }
        
        public static func trailing(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? trailing.equal(equalTo) : trailing.equal(equalTo).offset(offset!)
        }
        
        public static func topMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? topMargin.equal(equalTo) : topMargin.equal(equalTo).offset(offset!)
        }
        
        public static func leftMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? leftMargin.equal(equalTo) : leftMargin.equal(equalTo).offset(offset!)
        }
        
        public static func rightMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? rightMargin.equal(equalTo) : rightMargin.equal(equalTo).offset(offset!)
        }
        
        public static func lastBaseline(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? lastBaseline.equal(equalTo) : lastBaseline.equal(equalTo).offset(offset!)
        }
        
        public static func bottomMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? bottomMargin.equal(equalTo) : bottomMargin.equal(equalTo).offset(offset!)
        }
        
        public static func firstBaseline(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? firstBaseline.equal(equalTo) : firstBaseline.equal(equalTo).offset(offset!)
        }
        
        public static func leadingMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? leadingMargin.equal(equalTo) : leadingMargin.equal(equalTo).offset(offset!)
        }
        
        public static func trailingMargin(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? trailingMargin.equal(equalTo) : trailingMargin.equal(equalTo).offset(offset!)
        }
        
        public static func directionalEdges(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? directionalEdges.equal(equalTo) : directionalEdges.equal(equalTo).offset(offset!)
        }
        
        public static func directionalMargins(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? directionalMargins.equal(equalTo) : directionalMargins.equal(equalTo).offset(offset!)
        }
        
        public static func centerWithinMargins(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? centerWithinMargins.equal(equalTo) : centerWithinMargins.equal(equalTo).offset(offset!)
        }
        
        public static func centerXWithinMargins(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? centerXWithinMargins.equal(equalTo) : centerXWithinMargins.equal(equalTo).offset(offset!)
        }
        
        public static func centerYWithinMargins(_ equalTo: RelatableTarget, _ offset: OffsetTarget? = nil) -> Item {
            offset == nil ? centerYWithinMargins.equal(equalTo) : centerYWithinMargins.equal(equalTo).offset(offset!)
        }
    }
    
    enum Attributes {
        case top
        case size
        case width
        case edges
        case bottom
        case center
        case height
        case leading
        case centerX
        case centerY
        case margins
        case trailing
        case topMargin
        case leftMargin
        case rightMargin
        case lastBaseline
        case bottomMargin
        case firstBaseline
        case leadingMargin
        case trailingMargin
        case directionalEdges
        case directionalMargins
        case centerWithinMargins
        case centerXWithinMargins
        case centerYWithinMargins
    }
}

extension ElegantView {
    
    func convert(make: ConstraintMaker, layout: [Layout.Item]) {
        layout.forEach { (relatable) in
            switch relatable.attributes {
            case .width: make.width.equal(relatable.equalTo)
            case .height: make.height.equal(relatable.equalTo)
            case .top: make.top.equal(relatable.equalTo, relatable.offset)
            case .size: make.size.equal(relatable.equalTo, relatable.offset)
            case .edges: make.edges.equal(relatable.equalTo, relatable.offset)
            case .bottom: make.bottom.equal(relatable.equalTo, relatable.offset)
            case .center: make.center.equal(relatable.equalTo, relatable.offset)
            case .leading: make.leading.equal(relatable.equalTo, relatable.offset)
            case .centerX: make.centerX.equal(relatable.equalTo, relatable.offset)
            case .centerY: make.centerY.equal(relatable.equalTo, relatable.offset)
            case .margins: make.margins.equal(relatable.equalTo, relatable.offset)
            case .trailing: make.trailing.equal(relatable.equalTo, relatable.offset)
            case .topMargin: make.topMargin.equal(relatable.equalTo, relatable.offset)
            case .leftMargin: make.leftMargin.equal(relatable.equalTo, relatable.offset)
            case .rightMargin: make.rightMargin.equal(relatable.equalTo, relatable.offset)
            case .lastBaseline: make.lastBaseline.equal(relatable.equalTo, relatable.offset)
            case .bottomMargin: make.bottomMargin.equal(relatable.equalTo, relatable.offset)
            case .firstBaseline: make.firstBaseline.equal(relatable.equalTo, relatable.offset)
            case .leadingMargin: make.leadingMargin.equal(relatable.equalTo, relatable.offset)
            case .trailingMargin: make.trailingMargin.equal(relatable.equalTo, relatable.offset)
            case .directionalEdges: make.directionalEdges.equal(relatable.equalTo, relatable.offset)
            case .directionalMargins: make.directionalMargins.equal(relatable.equalTo, relatable.offset)
            case .centerWithinMargins: make.centerWithinMargins.equal(relatable.equalTo, relatable.offset)
            case .centerXWithinMargins: make.centerXWithinMargins.equal(relatable.equalTo, relatable.offset)
            case .centerYWithinMargins: make.centerYWithinMargins.equal(relatable.equalTo, relatable.offset)
            }
        }
    }
}

public extension ElegantView {
    
    @discardableResult
    func make(_ layout: [Layout.Item]) -> Self {
        snp.makeConstraints { convert(make: $0, layout: layout) }
        return self
    }
    
    @discardableResult
    func remake(_ layout: [Layout.Item]) -> Self {
        snp.remakeConstraints { convert(make: $0, layout: layout) }
        return self
    }
    
    @discardableResult
    func update(_ layout: [Layout.Item]) -> Self {
        snp.updateConstraints { convert(make: $0, layout: layout) }
        return self
    }
    
    @discardableResult
    func addSubview(_ view: ElegantView, layout: [Layout.Item]) -> Self {
        addSubview(view)
        view.make(layout)
        return self
    }
}

/// For Layout
public extension ElegantView {
    
    @discardableResult
    func make(_ layout: Layout) -> Self {
        snp.makeConstraints { convert(make: $0, layout: layout.relatableItems) }
        return self
    }
    
    @discardableResult
    func remake(_ layout: Layout) -> Self {
        snp.remakeConstraints { convert(make: $0, layout: layout.relatableItems) }
        return self
    }
    
    @discardableResult
    func update(_ layout: Layout) -> Self {
        snp.updateConstraints { convert(make: $0, layout: layout.relatableItems) }
        return self
    }
    
    @discardableResult
    func addSubview(_ view: ElegantView, layout: Layout) -> Self {
        addSubview(view)
        view.make(layout)
        return self
    }
}

public extension ElegantView {
    
    @available(iOS, deprecated: 16.0, message: "This's attribute will be remove.")
    @available(macOS, deprecated: 11.0, message: "This's attribute will be remove.")
    var layout: ConstraintViewDSL { snp }
    
    var top: ConstraintItem { snp.top }
    var size: ConstraintItem { snp.size }
    var width: ConstraintItem { snp.width }
    var edges: ConstraintItem { snp.edges }
    var height: ConstraintItem { snp.height }
    var bottom: ConstraintItem { snp.bottom }
    var center: ConstraintItem { snp.center }
    var leading: ConstraintItem { snp.leading }
    var centerX: ConstraintItem { snp.centerX }
    var centerY: ConstraintItem { snp.centerY }
    var margins: ConstraintItem { snp.margins }
    var trailing: ConstraintItem { snp.trailing }
    var topMargin: ConstraintItem { snp.topMargin }
    var leftMargin: ConstraintItem { snp.leftMargin }
    var rightMargin: ConstraintItem { snp.rightMargin }
    var lastBaseline: ConstraintItem { snp.lastBaseline }
    var bottomMargin: ConstraintItem { snp.bottomMargin }
    var firstBaseline: ConstraintItem { snp.firstBaseline }
    var leadingMargin: ConstraintItem { snp.leadingMargin }
    var trailingMargin: ConstraintItem { snp.trailingMargin }
    var directionalEdges: ConstraintItem { snp.directionalEdges }
    var directionalMargins: ConstraintItem { snp.directionalMargins }
    var centerWithinMargins: ConstraintItem { snp.centerWithinMargins }
    var centerXWithinMargins: ConstraintItem { snp.centerXWithinMargins }
    var centerYWithinMargins: ConstraintItem { snp.centerYWithinMargins }
}

extension ConstraintMakerExtendable {
    
    func equal(_ to: ConstraintRelatableTarget? = nil, _ offset: ConstraintOffsetTarget? = nil) {
        
        if let _ = to {
            if let _ = offset { equalTo(to!).offset(offset!)}
            else { equalTo(to!) }
        }else {
            if let _ = offset { equalToSuperview().offset(offset!)}
            else { equalToSuperview() }
        }
    }
}

public extension Array where Element == Layout.Item {
    
    static func + (lhs: [Element], rhs: Element) -> [Element] {
        return lhs + [rhs]
    }
}
