//
//  ViewController.swift
//  Example-macOS
//
//  Created by yaozuopan on 2024/4/3.
//  Copyright © 2024 Harvey/姚作潘. All rights reserved.
//

import Cocoa
import ElegantSnap
import RxSwift

import Foundation
 
func angleBetweenPoints(startPoint: CGPoint, endPoint: CGPoint) -> CGFloat {
    return atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x)
}

class ViewController: NSViewController {
    let disposeBag = DisposeBag()
    let label = UILabel()
    let inputView = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.wantsLayer()
        view.backgroundColor(NSColor("bg"))
        
        label
            .wantsLayer()
            .corner(20)
            .textColor(.white)
            .textAlignment(.center)
            .backgroundColor(.purple)
            .font(.boldSystemFont(ofSize: 16.0))
            .onTap(disposeBag) {
                print("-----")
            }
        
        inputView
            .wantsLayer()
            .superview(view)
            .font(.boldSystemFont(ofSize: 18))
            .border(.custom.color(.blue).thickness(3).radius(12))
            .layout([.leading.equal(20), .width.equal(220), .height(48), .bottom.offset(-130)])
            
        
        inputView.focusRingType = .none
        inputView.bezelStyle = .roundedBezel
        inputView.lineBreakMode = .byTruncatingTail

        view.addSubview(label, layout: [.height(60), .top.equal(10), .leading.equal(10), .trailing.equal(-10)])
        
        
        let comboBox = NSComboBox()
//        comboBox.cell = CustomComboBoxCell()
        comboBox.textColor(NSColor(0xE3EAFF)).font(.regular(14.0))
    
        comboBox
            .wantsLayer()
            .superview(view)
            .backgroundColor(.clear)
            .border(.custom.color(NSColor(0x3F80F4)).radius(11.5))
            .layout([
                .leading.equal(10),
                .top.equal(100),
                .height(23),
                .width(100)
            ])
        comboBox.focusRingType = .none
        comboBox.isButtonBordered = false
        comboBox.isSelectable = false
        comboBox.addItems(withObjectValues: ["Itme1", "Itme2", "Itme3"])
        
        let mySwitch = UISwitch()
        
        mySwitch.superview(view)
            .layout([
                .leading.equal(10),
                .top.equal(150),
                .height(21),
                .width(43)
            ])
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
//        // 使用示例
//        var point1 = CGPoint(x: 0, y: 99)
//        var point2 = CGPoint(x: 1, y: 0)
//        var angle = angleBetweenPoints(startPoint: point1, endPoint: point2)
//        print("夹角是: \(-angle * 180.0 * 2.0 / CGFloat.pi)")
//        
//        
//        // 使用示例
//        point1 = CGPoint(x: 0, y: 99)
//        point2 = CGPoint(x: 0, y: -99)
//        angle = angleBetweenPoints(startPoint: point1, endPoint: point2)
//        print("夹角是: \(-angle * 180.0 * 2.0 / CGFloat.pi)")
//
//        // 使用示例
//        point1 = CGPoint(x: 0, y: 99)
//        point2 = CGPoint(x: -1, y: 0)
//        angle = angleBetweenPoints(startPoint: point1, endPoint: point2)
//        print("夹角是: \(-angle * 180.0 * 2.0 / CGFloat.pi)")
//
//        // 使用示例
//        point1 = CGPoint(x: 0, y: 99)
//        point2 = CGPoint(x: 0, y: 1)
//        angle = angleBetweenPoints(startPoint: point1, endPoint: point2)
//        print("夹角是: \(-angle * 180.0 * 2.0 / CGFloat.pi)")
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}

extension ViewController: NSTextFieldDelegate {
    
}

class CustomComboBoxCell: NSComboBoxCell {
    
    override func drawInterior(withFrame cellFrame: NSRect, in controlView: NSView) {
        super.drawInterior(withFrame: cellFrame, in: controlView)
        print(Swift.type(of: controlView))
        
        if let popUp = controlView as? NSPopUpButton {
            if popUp.isHighlighted {
                // 设置高亮状态下的背景颜色
                NSColor.blue.setFill()
            } else {
                // 设置默认状态下的背景颜色
                NSColor.red.setFill()
            }
            
            let backgroundPath = NSBezierPath(rect: popUp.bounds)
            backgroundPath.fill()
        }
    }
}
