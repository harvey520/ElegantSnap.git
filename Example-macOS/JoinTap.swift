//
//  JoinTap.swift
//  JoinUI
//
//  Created by Harvey on 2023/11/26.
//

import Foundation
import RxSwift

public typealias ControlTapGesture = Observable<Void>

extension Reactive where Base: NSView {

    var tapGesture: ControlTapGesture {
        if self.base.isKind(of: NSButton.self) {
            fatalError("UIButton: Use `rx.tap` please.")
        }
        return controlTapGesture()
    }
    
    func controlTapGesture() -> ControlTapGesture {
        return Observable.create { [weak control = self.base] observer in
            MainScheduler.ensureRunningOnMainThread()
            
            guard let control = control else {
                observer.on(.completed)
                return Disposables.create()
            }
            
            let controlTapGesture = RXControlTapGesture(control) {
                observer.on(.next(()))
            }
            return Disposables.create(with: controlTapGesture.dispose)
        }
    }
}

class RXControlTapGesture: Disposable {

    private unowned var view: NSView
    private var callBack: () -> Void
    
    init(_ view: NSView, callBack: @escaping () -> Void) {

        self.view = view
        self.callBack = callBack
        self.view.addGestureRecognizer(NSClickGestureRecognizer(target: self, action: #selector(tapGestureRecognizer)))
    }
    
    @objc private func tapGestureRecognizer() {
        callBack()
    }
    
    func dispose() { }
}
