//
//  AttributedString.swift
//  MyBase
//
//  Created by Harvey on 2021/5/18.
//

import AppKit

public typealias AttributedString = NSMutableAttributedString

public extension AttributedString {
    
    @discardableResult
    func add(_ item: Item) -> Self {
        item.texts.forEach { text in
            addAttributes(item.values, range: string.rangeOf(text))
        }
        return self
    }
    
    @discardableResult
    func add(_ attributes: Attributes) -> Self {
        addAttributes(attributes.values, range: string.range)
        return self
    }
}

public extension AttributedString {
    class Item: Attributes {
        var texts: [String] = []
        
        public func text(_ text: String) -> Self {
            self.texts = [text]
            return self
        }
        
        public func texts(_ texts: [String]) -> Self {
            self.texts = texts
            return self
        }
        
        public static var item: Item { Item() }
    }
}

public class Attributes {
    var values: [NSAttributedString.Key: Any] = [:]
    
    public static var attributes: Attributes { Attributes() }
}

public extension Attributes {
    
    @discardableResult
    func font(_ value: NSFont) -> Self {
        values[.font] = value
        return self
    }

    /// NSParagraphStyle, default defaultParagraphStyle
    @discardableResult
    func paragraphStyle(_ value: NSMutableParagraphStyle) -> Self {
        values[.paragraphStyle] = value
        return self
    }

    /// NSColor, default blackColor
    @discardableResult
    func foregroundColor(_ value: NSColor) -> Self {
        values[.foregroundColor] = value
        return self
    }

    /// NSColor, default nil: no background
    @discardableResult
    func backgroundColor(_ value: NSColor) -> Self {
        values[.backgroundColor] = value
        return self
    }

    /// NSNumber containing integer, default 1: default ligatures, 0: no ligatures
    @discardableResult
    func ligature(_ value: Int) -> Self {
        values[.ligature] = value
        return self
    }

    /// NSNumber containing floating point value, in points; amount to modify default kerning. 0 means kerning is disabled.
    @discardableResult
    func kern(_ value: Float) -> Self {
        values[.kern] = value
        return self
    }

    /// NSNumber containing floating point value, in points; amount to modify default tracking. 0 means tracking is disabled.
    @available(iOS 14.0, *)
    @discardableResult
    func tracking(_ value: Float) -> Self {
        values[.tracking] = value
        return self
    }

    /// NSNumber containing integer, default 0: no strikethrough
    @discardableResult
    func strikethroughStyle(_ value: Int) -> Self {
        values[.strikethroughStyle] = value
        return self
    }

    /// NSNumber containing integer, default 0: no underline
    @discardableResult
    func underlineStyle(_ value: NSUnderlineStyle) -> Self {
        values[.underlineStyle] = value.rawValue
        return self
    }

    /// NSColor, default nil: same as foreground color
    @discardableResult
    func strokeColor(_ value: NSColor) -> Self {
        values[.strokeColor] = value
        return self
    }

    /// NSNumber containing floating point value, in percent of font point size, default 0: no stroke; positive for stroke alone, negative for stroke and fill (a typical value for outlined text would be 3.0)
    @discardableResult
    func strokeWidth(_ value: Float) -> Self {
        values[.strokeWidth] = value
        return self
    }

    /// NSShadow, default nil: no shadow
    @discardableResult
    func shadow(_ value: NSShadow) -> Self {
        values[.shadow] = value
        return self
    }

    /// NSString, default nil: no text effect
    @discardableResult
    func textEffect(_ value: String) -> Self {
        values[.textEffect] = value
        return self
    }

    /// NSTextAttachment, default nil
    @discardableResult
    func attachment(_ value: NSTextAttachment) -> Self {
        values[.attachment] = value
        return self
    }

    /// NSURL (preferred) or NSString
    @discardableResult
    func link(_ value: String) -> Self {
        values[.link] = value
        return self
    }

    /// NSNumber containing floating point value, in points; offset from baseline, default 0
    @discardableResult
    func baselineOffset(_ value: Float) -> Self {
        values[.baselineOffset] = value
        return self
    }

    /// NSColor, default nil: same as foreground color
    @discardableResult
    func underlineColor(_ value: NSColor) -> Self {
        values[.underlineColor] = value
        return self
    }

    /// NSColor, default nil: same as foreground color
    @discardableResult
    func strikethroughColor(_ value: NSColor) -> Self {
        values[.strikethroughColor] = value
        return self
    }
}

public extension String {
    
    var range: NSRange {
        NSRange(location: 0, length: count)
    }
    
    func rangeOf(_ subString: String) -> NSRange {
        return (self as NSString).range(of: subString)
    }
    
    @discardableResult
    func add(_ item: AttributedString.Item) -> AttributedString {
        return AttributedString(string: self).add(item)
    }
    
    @discardableResult
    func add(_ attributes: Attributes) -> AttributedString {
        return AttributedString(string: self).add(attributes)
    }
}

public extension String {
    class Size {
        var value: CGSize = CGSize(width: CGFloat.infinity, height: CGFloat.infinity)
        
        @discardableResult
        public func width(_ w: CGFloat) -> Self {
            value.width = w
            return self
        }
        
        @discardableResult
        public func height(_ h: CGFloat) -> Self {
            value.height = h
            return self
        }
        
        public static var fit: Size { Size() }
    }
    
    /// Calculate and returns the size necessary to draw the string that best fits the specified size.
    func size(_ fit: Size,
              attrs: Attributes,
              options: NSString.DrawingOptions = .usesLineFragmentOrigin) -> CGSize {
        add(attrs)
            .boundingRect(with: fit.value, options: options, context: nil)
            .size.ceil
    }
}

public extension CGSize {
    var ceil: CGSize {
        CGSize(width: Darwin.ceil(width), height: Darwin.ceil(height))
    }
}
