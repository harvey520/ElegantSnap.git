//
//  UISwitch.swift
//  Example-macOS
//
//  Created by yaozuopan on 2024/4/7.
//  Copyright © 2024 Harvey/姚作潘. All rights reserved.
//

import AppKit
import RxSwift
import ElegantSnap

class UISwitch: NSImageView {
    
    var isOn: Bool = false {
        didSet {
            image = NSImage(named: isOn ? "s_s_h" : "s_s_n")
            updateUI()
        }
    }
    
    private let markView = NSImageView(image: NSImage(named: "s_s_t")!)
    
    private let disposeBag = DisposeBag()
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
    
        image = NSImage(named: "s_s_n")
        imageAlignment = .alignCenter
        clipsToBounds = false
        
        markView
            .superview(self)
        
           updateUI()
        
        self.onTap(disposeBag) { [unowned self] in
            isOn = !isOn
        }
    }
    
    private func updateUI() {
        markView.remake([
            .top.equal(-8),
            .size(30, 37),
            isOn ? .trailing : .leading
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
