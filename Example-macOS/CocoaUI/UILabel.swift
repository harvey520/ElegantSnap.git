//
//  NSLabel.swift
//  Example-macOS
//
//  Created by Harvey on 2024/4/3.
//  Copyright © 2024 Harvey/姚作潘. All rights reserved.
//

import Foundation
import AppKit

class UILabel: NSView {
    lazy var label: NSTextField = {
        .init(labelWithString: "label")
    }()
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        label
            .superview(self)
            .layout([
                .leading,
                .centerY,
                .trailing,
                .height(18.0)
            ])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension UILabel {
    
    @discardableResult
    func textColor(_ value: NSColor) -> Self {
        label.textColor = value
        return self
    }
    
    @discardableResult
    func font(_ value: NSFont) -> Self {
        label.font = value
        calculate()
        return self
    }
    
    @discardableResult
    func textAlignment(_ value: NSTextAlignment) -> Self {
        label.alignment = value
        return self
    }
    
    @discardableResult
    func text(_ value: String?) -> Self {
        label.stringValue = value ?? ""
        calculate()
        return self
    }
    
    /// 计算绘制字符串的size 和更新布局
    private func calculate() {
        guard let font = label.font, label.stringValue.count > 0 else { return }
        let size = label.stringValue.size(.fit, attrs: .attributes.font(font))
        label.update([.height(size.height)])
    }
}

public extension NSFont {
    static func regular(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .regular)
    }
    
    static func semibold(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .semibold)
    }
    
    static func medium(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .medium)
    }
    
    static func bold(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .bold)
    }
    
    static func thin(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .thin)
    }
    
    static func ultraLight(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .ultraLight)
    }
    
    static func heavy(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .heavy)
    }
    
    static func black(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .black)
    }
    
    static func light(_ size: CGFloat) -> NSFont {
        return .systemFont(ofSize: size, weight: .light)
    }
}
