//
//  NSTextField.swift
//  Example-macOS
//
//  Created by yaozuopan on 2024/4/7.
//  Copyright © 2024 Harvey/姚作潘. All rights reserved.
//

import AppKit

class UITextField: NSTextField {
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
}

extension NSTextField {
    
    @discardableResult
    func textColor(_ value: NSColor) -> Self {
        textColor = value
        return self
    }
    
    @discardableResult
    func font(_ value: NSFont) -> Self {
        font = value
        return self
    }
    
    @discardableResult
    func textAlignment(_ value: NSTextAlignment) -> Self {
        alignment = value
        return self
    }
    
    @discardableResult
    func text(_ value: String?) -> Self {
        stringValue = value ?? ""
        
        return self
        
    }
    @discardableResult
    func placeholder(_ value: Placeholder) -> Self {
        let attributedString = NSMutableAttributedString(string: value.text)
        attributedString.setAttributes([.foregroundColor: value.color,
                                        .font: value.font
        ],
                                       range: value.text.range)
        placeholderAttributedString = attributedString
        return self
    }
}

extension NSTextField {
    
    @discardableResult
    func bezelStyle(_ value: NSTextField.BezelStyle) -> Self {
        self.bezelStyle = value
        return self
    }
    
    @discardableResult
    func allowsEditingTextAttributes(_ value: Bool) -> Self {
        allowsEditingTextAttributes = value
        return self
    }
}

public class Placeholder {
    
    var text: String = ""
    var color: NSColor = .black
    var font: NSFont = NSFont.systemFont(ofSize: 12)
    
    public init() { }
    
    @discardableResult
    public func color(_ value: NSColor) -> Self {
        color = value
        return self
    }
    
    @discardableResult
    public func font(_ value: NSFont) -> Self {
        font = value
        return self
    }
    
    @discardableResult
    public func text(_ value: String) -> Self {
        text = value
        return self
    }
}
