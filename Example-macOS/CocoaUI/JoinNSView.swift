//
//  JoinNSView.swift
// JoinUI
//
//  Created by Harvey on 2023/11/24.
//

import AppKit
import ElegantSnap
import RxCocoa
import RxSwift

public extension NSView {
    
    @discardableResult
    func wantsLayer(_ value: Bool = true) -> Self {
        wantsLayer = value
        return self
    }
    
    @discardableResult
    func backgroundColor(_ value: NSColor) -> Self {
        layer?.backgroundColor = value.cgColor
        return self
    }
    
    /// 处理圆角
    ///
    /// - Parameter radius: 半径
    /// - Returns: NSView
    @discardableResult
    func corner(_ radius: Double) -> Self {
        layer?.cornerRadius = radius
        return self
    }
    
    @discardableResult
    func masksToBounds(_ value: Bool) -> Self {
        layer?.masksToBounds = value
        return self
    }
    
    /// 超出父视图的内容是否被裁剪
    @discardableResult
    func clip(_ value: Bool) -> Self {
        layer?.masksToBounds = value
        return self
    }
    
    @discardableResult
    func opacity(_ value: Float) -> Self {
        layer?.opacity = value
        return self
    }
    
    @discardableResult
    func superview(_ value: NSView) -> Self {
        value.addSubview(self)
        return self
    }
    
    @discardableResult
    func layout(_ value: [Layout.Item]) -> Self {
        self.make(value)
        return self
    }
    
    @discardableResult
    func layout(_ value: Layout) -> Self {
        self.make(value)
        return self
    }
    
    @discardableResult
    func shadow(_ value: Shadow) -> Self {
        layer?.shadowOffset = value.offset
        layer?.shadowColor = value.color.cgColor
        layer?.shadowOpacity = value.opacity
        layer?.shadowRadius = value.radius
        return self
    }
    
    @discardableResult
    func frame(_ value: CGRect) -> Self {
        self.frame = value
        return self
    }
    
    @discardableResult
    func size(_ value: CGSize) -> Self {
        self.frame = CGRect(origin: .zero, size: value)
        return self
    }
    
    @discardableResult
    func border(_ value: Border) -> Self {
        layer?.borderColor = value.color.cgColor
        layer?.borderWidth = value.thickness
        layer?.cornerRadius = value.radius
        return self
    }
    
    @discardableResult
    func addSubviews(_ views: [NSView]) -> Self {
        views.forEach { addSubview($0) }
        return self
    }
    
    @discardableResult
    func onTap(_ bag: DisposeBag, action: @escaping () -> Void) -> Self {
        guard self.isMember(of: NSButton.self) else {
            self.rx.tapGesture.subscribe(onNext: action).disposed(by: bag)
            return self
        }
        (self as! NSButton).rx.tap.subscribe(onNext: action).disposed(by: bag)
        return self
    }
}

public extension NSView {
    
    @discardableResult
    func bounds(_ value: CGRect) -> Self {
        bounds = value
        return self
    }
    
    @discardableResult
    func autoresizesSubviews(_ value: Bool) -> Self {
        autoresizesSubviews = value
        return self
    }
    
    @discardableResult
    func autoresizingMask(_ value: NSView.AutoresizingMask) -> Self {
        autoresizingMask = value
        return self
    }
    
    @discardableResult
    func clipsToBounds(_ value: Bool) -> Self {
        clipsToBounds = value
        return self
    }
    
    @discardableResult
    func alpha(_ value: Float) -> Self {
        layer?.opacity = value
        return self
    }
    
    @discardableResult
    func hidden(_ value: Bool) -> Self {
        isHidden = value
        return self
    }
    
    @discardableResult
    func translatesAutoresizingMaskIntoConstraints(_ value: Bool) -> Self {
        translatesAutoresizingMaskIntoConstraints = value
        return self
    }
    
    /// The layer’s position on the z axis. Animatable.
    @discardableResult
    func zPosition(_ value: CGFloat) -> Self {
        layer?.zPosition = value
        return self
    }
}

public protocol JoinSaveInstance { }

public extension JoinSaveInstance where Self: NSView {
    
    @discardableResult
    func save(to varName: inout Self?) -> Self {
        varName = self
        return self
    }
}
extension NSView: JoinSaveInstance { }

public extension NSView {
    
    typealias LineData = (start: CGPoint, end: CGPoint)
    
    struct DrawConfig {
        public typealias DashPattern = (dotWidth: CGFloat, dotSpacing: CGFloat)
        
        public var fillColor: NSColor
        public var strokeColor: NSColor?
        public var lineWidth: CGFloat
        public var fillRule: CAShapeLayerFillRule
        public var lineCap: CAShapeLayerLineCap
        public var lineJoin: CAShapeLayerLineJoin
        public var lineDashPattern: DashPattern?
        
        public init(fillColor: NSColor = .black,
                    strokeColor: NSColor? = nil,
                    lineWidth: CGFloat = 1.0,
                    fillRule: CAShapeLayerFillRule = .nonZero,
                    lineCap: CAShapeLayerLineCap = .butt,
                    lineJoin: CAShapeLayerLineJoin = .round,
                    lineDashPattern: DashPattern? = nil) {
            self.fillColor = fillColor
            self.strokeColor = strokeColor
            self.lineWidth = lineWidth
            self.fillRule = fillRule
            self.lineCap = lineCap
            self.lineJoin = lineJoin
            self.lineDashPattern = lineDashPattern
        }
        
        var dashPattern: [NSNumber]? {
            guard let lineDashPattern = lineDashPattern else { return nil }
            return [NSNumber(value: lineDashPattern.dotWidth), NSNumber(value: lineDashPattern.dotSpacing)]
        }
        
        public static let `default` = DrawConfig()
    }
}

public protocol JoinLockProperty { }

public extension JoinLockProperty where Self: NSObject {
    
    /// 锁定某个属性为某个状态，持续一段时间后自动解锁
    ///
    /// - Parameters:
    ///   - id: keyPath
    ///   - state: 锁定状态
    ///   - duration: 持续时间
    ///   - unlockState: 解锁状态
    func lock<Value>(_ id: WritableKeyPath<Self, Value>,
                     to state: Value,
                     duration: TimeInterval,
                     unlock unlockState: Value) {
        var mutatingSelf = self
        mutatingSelf[keyPath: id] = state
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) { [weak self] in
            guard var mutatingSelf = self else { return }
            mutatingSelf[keyPath: id] = unlockState
        }
    }
}

extension NSView: JoinLockProperty { }

public protocol JoinTimerDisposable { }

public extension JoinTimerDisposable {
    
    /// 启动一个定时器
    ///
    /// - Parameters:
    ///   - dueTime: 产生第一个值的相对时间, 单位：秒,默认0
    ///   - period: 隔多长时间回调一次，单位：秒，默认1
    ///   - duration: 持续时间, 单位：秒
    ///   - periodBlock: 多次回调
    /// - Returns: 定时器可销毁资源
    ///
    /// - Example
    ///
    /// ```swift
    /// // 你应该定义一个变量，持有 `timer`
    /// let timer: Disposable = view.launchTimer(1, duration: 10) { time, isEnd in
    ///    if isEnd {
    ///         print("it's end, do something")
    ///         return
    ///     }
    ///     print("do other something")
    /// }
    ///
    /// // 如果不再需要, 应该销毁
    /// timer.dispose()
    /// ```
    func launchTimer(_ dueTime: Int = 0,
                     period: Int = 1,
                     duration: Int,
                     periodBlock: @escaping ((_ duration: Int, _ isEnd: Bool) -> Void)) -> Disposable {
        return Observable<Int>.timer(
            .seconds(dueTime),
            period: .seconds(period),
            scheduler: ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global()))
        .take(duration)
        .subscribe(onNext: { value in
            if (value + 1) == duration {
                return periodBlock(value, true)
            }
            periodBlock(value, false)
        })
    }
}

extension NSView: JoinTimerDisposable { }

public class ViewPath {
    fileprivate var classNames: [String] = []
    
    @discardableResult
    public func next(_ className: String) -> Self {
        classNames.append(className)
        return self
    }
    
    public static var path: ViewPath { .init() }
}

public extension NSView {
    
    func seekSubViews(of path: ViewPath) -> [NSView] {
        return seekSubView("-" + path.classNames.joined(separator: "-"))
    }
    
    private func seekSubView(_ path: String, currentPath: String = "") -> [NSView] {
        if currentPath == path {
            return [self]
        }
        
        var result: [NSView] = []
        subviews.forEach { item in
            result.append(contentsOf: item.seekSubView(path, currentPath: currentPath + "-" + item.className))
        }
        
        return result
    }
}

public class Border {
   
    var thickness: CGFloat = 1.0
    var color: NSColor = .black
    var radius: CGFloat = 0.0
    
    /// 边框厚度
    @discardableResult
    public func thickness(_ value: CGFloat) -> Self {
        thickness = value
        return self
    }
    
    @discardableResult
    public func color(_ value: NSColor) -> Self {
        color = value
        return self
    }
    
    @discardableResult
    public func radius(_ value: CGFloat) -> Self {
        radius = value
        return self
    }
    
    public static var custom: Border { Border() }
}

public class Shadow {
    public static var custom: Shadow {
        .init()
    }
    
    private(set) var radius: CGFloat  = 3.0
    private(set) var offset: CGSize  = CGSize(width: 0, height: -3)
    private(set) var color: NSColor  = .black
    private(set) var opacity: Float  = 0.0
    
    public init() {}
    
    @discardableResult
    public func radius(_ value: CGFloat) -> Self {
        radius = value
        return self
    }
    
    @discardableResult
    public func offset(_ value: CGSize) -> Self {
        offset = value
        return self
    }
    
    @discardableResult
    public func color(_ value: NSColor) -> Self {
        color = value
        return self
    }
    
    @discardableResult
    public func opacity(_ value: Float) -> Self {
        opacity = value
        return self
    }
}

extension NSColor {
    /// 十六进制颜色 e.g. 0xFFFF99
    /// - Parameters:
    ///   - value: 十六进制颜色
    ///   - alpha: 不透明度，范围 [0.0, 1.0], 默认为 1.0
    convenience init(_ hexValue: UInt, alpha: Float = 1.0) {
        
        let red = (hexValue & 0xFF0000) >> 16
        let green = (hexValue & 0x00FF00) >> 8
        let blue = hexValue & 0x0000FF
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha))
    }
    
    func alpha(_ alpha: CGFloat) -> NSColor {
        return withAlphaComponent(alpha)
    }
    
    /// Creates a color object that uses the specified image pattern to paint the target area.
    convenience init(_ named: String) {
        self.init(patternImage: NSImage.init(named: named)!)
    }
}
